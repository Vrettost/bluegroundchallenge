package config;

public class WeatherURLConfiguration {

    private static final String API_KEY = "5262b0b535dbc0d0";

    private static final String DATE = "20171030";

    private static final String CITY = "New_York";

    private static final String BASE_URI = "http://api.wunderground.com/api/";

    private static final String STATE = "NY";

    public static final String URI_HISTORY = BASE_URI + API_KEY + "/history_" + DATE + "/q/" + STATE + "/" + CITY + ".json";


}
