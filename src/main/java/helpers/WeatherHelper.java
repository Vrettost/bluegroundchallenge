package helpers;


import config.WeatherURLConfiguration;
import model.Dailysummary;
import model.WeatherResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

public class WeatherHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherHelper.class);

    private RestTemplate restTemplate = new RestTemplate();




    private WeatherResult getWeatherResult(String URI){

        WeatherResult wr = null;

        try{
            wr = restTemplate.getForObject(URI, WeatherResult.class);
        }catch(Exception ex){
            LOGGER.error(ex.getMessage());
        }

        return wr;
    }


    public String createMetricsTable(){
        WeatherResult wr = getWeatherResult(WeatherURLConfiguration.URI_HISTORY);
        String metricsTable = null;

        if(wr != null && wr.getHistory() != null && wr.getHistory().getDailysummary() != null && wr.getHistory().getDailysummary().size() > 0){
            Dailysummary ds = wr.getHistory().getDailysummary().get(0);

            metricsTable = "Max percentage humidity: " + ds.getMaxhumidity() + "%\n" +
                    "Max Temperature: " + ds.getMaxtempm() + " Degrees Celsius\n" +
                    "Min Temperature: " + ds.getMintempm() + " Degrees Celsius\n" +
                    "Precipitation: " + ds.getPrecipm() + "mm";
        }

        return metricsTable;
    }
    




}
