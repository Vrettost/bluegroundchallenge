import helpers.WeatherHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class BlueGroundChallengeApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlueGroundChallengeApplication.class);

    private static final String FILENAME = System.getProperty("user.home") + "/Desktop/Metrics.txt";


    public static void main(String args[]){
        WeatherHelper helper = new WeatherHelper();
        BufferedWriter br = null;

        try{
            //Make the REST call, get the data, extract it into the model objects and create the metrics table
            final String metricsTableContent = helper.createMetricsTable() != null ? helper.createMetricsTable() : "Something went bad, nothing to print!";

            //Initiate the process to write the extracted content into the file
            File f =  new File(FILENAME);

            if(f.exists()){
                f.createNewFile();
            }

            br = new BufferedWriter(new FileWriter(f));

            br.write(metricsTableContent);

        }catch(IOException ioexc){
            LOGGER.error(ioexc.getMessage());
        }finally{
            try{
                //Close buffered Writer
                br.close();
            }catch(Exception ex){
                LOGGER.error(ex.getMessage());
            }
        }
    }
}
